# Network Checker -console app
> This is console application for checking network connectivity. 
As for now you can check icmp and port access to remote hosts.

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Screenshots](#screenshots)
* [Setup](#setup)
* [Usage](#usage)
* [Project Status](#project-status)
* [Room for Improvement](#room-for-improvement)
* [Acknowledgements](#acknowledgements)
* [Contact](#contact)
<!-- * [License](#license) -->


## General Information
This is console application that provide ability to ping and check port access to remote hosts.  
App accept single hostname as argument or file with multiple hosts in seperate lines.
In input we also specify which fuction we want to check (icmp or socket).  
With this app you can fast check connection to multiple hosts.  
I've made this program to expand my programming skills and use it at work.  


## Technologies Used
- Python 3.10


## Features
List the ready features here:
- ICMP checking
- Remote ports cheking
- Input from file


## Screenshots
![Example screenshot](./program.png)
<!-- If you have screenshots you'd like to share, include them here. -->


## Setup
Clone this repo to your desktop and run `pip install -r ./requirements.txt` to install all the dependencies.


## Usage
Once the dependencies are installed you can run `main.py -h` to check required input parameters  

`main.py -mode "ping" -host "hostname"`  
`main.py -mode "ping" -host "input_file"`  
`main.py -mode "ssh" -host "hostname"`  
`main.py -mode "ssh" -host "input_file"`  


## Project Status
Project is: _in progress_


## Room for Improvement

Room for improvement:
- Add more tests
- Add more functionality

To do:
- Feature to be added 1
- Feature to be added 2

from setuptools import setup


#python setup.py sdist
setup(
    name='network_checker',
    version='0.1',
    description='App for checking network connectivity',
    author='Przemyslaw Kozak',
    author_email='przem.pg@gmail.com',
    packages=['network_checker'],
    install_requires=[
        'paramiko',
        'pythonping',
        'argparse'
        ],
    url='https://gitlab.com/reksiooo/network-checker'
)
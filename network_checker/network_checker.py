"""App for checking ping and ssh connection"""
from contextlib import closing
from pythonping import ping
import os
import logging
import socket
import argparse
import sys
import paramiko


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class ValidationError(Exception):
    """Custom exception class"""


class Pingator():
    """Class iplementing ping function"""
    def __init__(self, host) -> None:
        self.host = host

    def is_pinging(self):
        """Check if host is pinging

        Returns:
            bool: True or False
        """
        try:
            for response in ping(self.host, count=2):
                if str(response) == "Request timed out":
                    return False
            return True
        except RuntimeError as error:
            return error


def logger():
    loglevel = os.environ.get('LOGLEVEL', 'DEBUG')
    logging.basicConfig(filename='app.log', filemode='a', format='%(asctime)s - %(levelname)s - %(message)s', level=loglevel)

    
    logging.info(f'Loglevel is set to {logging.getLevelName(logging.root.level)}')
    logging.warning('This will get logged to a file')
    logging.warning('This will get logged to a file123')


def check_ssh(host):
    """Class implementing ssh function - still in construction!"""
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    try:
        ssh.connect(hostname=host)
        return True
    except:
        return False
    finally:
        ssh.close()


def check_socket(host, port):
    """Method for checking port connection

    Args:
        host (string): remote hostname you want to check
        port (int): port number you want to check

    Returns:
        _type_: _description_
    """
    try:
        with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
            if sock.connect_ex((host, port)) == 0:
                return True
            return False
    except OSError as error:
        if error.errno == 11001:
            print(f'Cannot resolve address "{host}", try verify your DNS or host file!')
            sys.exit(1)
        return None



# with open('rlub.txt', mode='r') as file:
#     for line in file:
#         host = line.strip()
#         check = ping(host, count=1)
#         print(f'{host}: {check._responses[0]}')

# with open('rlub.txt', mode='r') as file:
#     for line in file:
#         host = line.strip()
#         check = check_ssh(host)
#         print(f'SSH to host {host}: {check}')

# host = "sgda-sud2"
# check = check_ssh(host)
# print(f'SSH to host {host}: {check}')

def main():
    logger()
    logging.info('Application started')


    """Main method of program"""
    parser = argparse.ArgumentParser(description='Program for checking connectivity to hosts')
    #group = parser.add_mutually_exclusive_group()  # we can only specify one argument at time

    #  Add argument to parser method() and exclusive group
    parser.add_argument("--host", "-host", help="Enter host IP or DNS name")
    parser.add_argument("--file", "-file", help="Enter text file name with hosts IP or DNS names")
    parser.add_argument("--mode", '-mode', help='Enter type of check: ping or ssh')

    #  Execute the parse_args() method
    args = parser.parse_args()

    #  If user not specified any input argument print help and exit
    if len(sys.argv) == 1:
        logging.info('App launched without input arguments. Show help info')
        parser.print_help()
        input("Press enter to exit")
        parser.exit()

    if args.host:  # User specified ip argument
        hosts = [args.host]
    if args.file:  # User specified file argument
        try:
            with open(args.file, encoding="utf8") as file:  # read hosts from file and save to array
                hosts = [line.rstrip() for line in file]
        except FileNotFoundError:
            print(f'File {args.file} not found!')
            sys.exit(1)
        except PermissionError:
            print(f'Permission denied to location {args.file}!')
            sys.exit(1)
        except OSError as error:
            if error.errno == 22:
                print(f'Invalid argument in input filename: "{args.file}"!')
            sys.exit(1)

    if args.mode == "ping":
        for host in hosts:
            pingam = Pingator(host)
            wynik = pingam.is_pinging()
            if wynik == False:
                print(f'Ping to host {host}: {bcolors.FAIL}{wynik}{bcolors.ENDC}')
            else:
                print(f'Ping to host {host}: {wynik}')

    if args.mode == "ssh":
        for host in hosts:
            check = check_socket(host, 22)
            if check == False:
                print(f'SSH to host {host}: {bcolors.FAIL}{check}{bcolors.ENDC}')
            else:
                print(f'SSH to host {host}: {check}')


if __name__ == "__main__":
    main()
    input("Press enter to exit")

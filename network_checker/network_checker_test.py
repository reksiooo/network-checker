from network_checker import Pingator, check_socket


def test_if_host_is_pinging_positive():
    #given
    ping_check = Pingator('127.0.0.1')

    #when
    result = ping_check.is_pinging()
    
    #then
    assert result is True


def test_if_host_is_pinging_negative():
    #given
    ping_check = Pingator('2.54.23.2')

    #when
    result = ping_check.is_pinging()

    #then
    assert result is False


def test_if_bad_hostname_given():
    #given
    ping_check = Pingator('999.999.999.999')

    #when
    result = ping_check.is_pinging()

    #then
    assert 'Cannot resolve address' in str(result)


def test_if_port_is_open_positive():
    #given when
    result = check_socket('wp.pl', 80)

    #then
    assert result is True


def test_if_port_is_open_negative():
    #when
    result = check_socket('wp.pl', 22)

    #then
    assert result is False
